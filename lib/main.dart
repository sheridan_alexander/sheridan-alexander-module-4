import 'package:flutter/material.dart';
import 'dashboard.dart';
import 'features.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:page_transition/page_transition.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'First App',
      home: SplashScreen(),
      debugShowCheckedModeBanner:false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key,}) : super(key: key);




  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  
  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      backgroundColor:Colors.white,
      appBar: AppBar(
        
        title: const Text(
          'UWC Library Assistant',
          style: TextStyle(
            fontSize: 60,
            fontWeight: FontWeight.bold,
            color: Colors.white,
          ),
        ),
        bottom: PreferredSize(
          preferredSize: const Size.fromHeight(80),
          child: Container(
            height: 80,
            color:Colors.white,
            child: const Center(
              child: Text(
                'Login',
                style: TextStyle(
                  fontSize: 40,
                  fontWeight: FontWeight.bold,
                  color: Colors.brown,
                ),
              ),
            ),
          ),
        ),
        backgroundColor: Colors.brown,
        leading:const Icon(
          Icons.library_books,
          color:Colors.white,
          size:40,
        ),
      ),
      body: SizedBox(
        width: double.infinity,
        child: Padding(
          padding: const EdgeInsets.only(left: 80,right: 80 ),
          child: Column(
            children: [
              const SizedBox(
                height: 30,
              ),
              _labelTextInput("Username","Enter username here",false),
              const SizedBox(
                height: 70,
              ),
              _labelTextInput("Password","Enter password here",true),
              const SizedBox(
                height: 150,
              ),
              _loginBtn(context),
              const SizedBox(
                height: 20,
              ),
              _signUpLabel("Don't have an account yet?", 10.0,FontWeight.w400,Colors.black,context),
              _signUpLabel("Sign Up",15.0, FontWeight.w800,Colors.brown,context),
              const SizedBox(
                height: 5,
              ),
            ],
          ),
        ), 
      ), 
    );
  }
}

class SplashScreen extends StatelessWidget{
  const SplashScreen({Key? key}):super(key:key);

  @override
  Widget build(BuildContext context){
    return AnimatedSplashScreen(
      splash:const Icon(
        Icons.library_books,
        color:Colors.brown,
        size:150,
      ),
      backgroundColor: Colors.white,
      nextScreen: const MyHomePage(),
      duration:4000,
      splashTransition: SplashTransition.slideTransition,
      pageTransitionType:PageTransitionType.leftToRight,
    );
  }
}

Widget _loginBtn(BuildContext context){
  return Container(
    width: 300,
    height: 60,
    decoration: const BoxDecoration(
      color: Colors.brown,
      borderRadius: BorderRadius.all(Radius.circular(10)),
    ),
    child: TextButton(
      onPressed: () => {
        Navigator.push(context, MaterialPageRoute(
          builder: (context) => const Dashboard())),
      },
      child: const Text(
        "Login",
        style: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.bold,
          fontSize: 24,
        ),
      ),
    ),
  );
}


Widget _labelTextInput(String label,String hintText,bool isPassed){
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Text(
        label,
        style: const TextStyle(
          color: Colors.brown,
          fontWeight: FontWeight.bold,
          fontSize: 20,
        ),
      ),
      TextField(
        style:const TextStyle(color: Colors.black),
        obscureText: isPassed,
        cursorColor: Colors.black,
        decoration: InputDecoration(
          hintText:hintText,
          hintStyle: const TextStyle(
            color: Colors.grey,
            fontSize:20,
            fontWeight:FontWeight.w100,
          ),
          enabledBorder: const UnderlineInputBorder(
            borderSide: BorderSide(
              color:Colors.white
            ),
          ),
        ),
      ),
    ],
  );
}


Widget _signUpLabel(String text,double fSize,FontWeight weight,Color cText, BuildContext context){
  return InkWell(
    onTap:(){
      Navigator.push(context, MaterialPageRoute(
        builder: (context) => const Register()));
    },
    child:Text(
      text,
      style:TextStyle(
        fontSize: fSize,
        fontWeight: weight,
        color: cText, 
      ),
    ),
  ); 
}
