import 'package:flutter/material.dart';
import 'features.dart';

class Dashboard extends StatelessWidget{
  const Dashboard({Key? key}) : super(key: key);

   @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar:AppBar(
        title: const Text(
          'Dashboard',
          style: TextStyle(
            fontSize: 40,
            fontWeight: FontWeight.bold,
            color: Colors.white,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.brown,
      ),
      floatingActionButton:FloatingActionButton(
        onPressed: () => {
          Navigator.push(context,MaterialPageRoute(
              builder: (context) => const Chat())),
        },
        splashColor: Colors.white,
        backgroundColor: Colors.blueAccent,
        child:const Icon(Icons.message),
      ),
      body:Container(
        padding: const EdgeInsets.all(30.0),
        child:GridView.count(
          crossAxisCount:3,
          children:<Widget>[
            _menuIcon("Search For Book", Icons.search,Colors.white,context,MaterialPageRoute(builder: (context)=> const Search())),
            _menuIcon("View Operating Times", Icons.info,Colors.white,context,MaterialPageRoute(builder: (context)=> const Operation())),
            _menuIcon("Edit Profile", Icons.person,Colors.white,context,MaterialPageRoute(builder: (context)=> const Edit())),
          ],
        ),
      ),
    );
  }  
}


Widget _menuIcon(String label,IconData iconName,Color color,BuildContext context,Route route){
  return Card(
    color: Colors.brown,
    margin: const EdgeInsets.all(8.0),
              child:InkWell(
                onTap: ()=>{
                  Navigator.push(context,route),
                },
                splashColor:Colors.white,
                child: Center(
                  child: Column(
                    children: <Widget>[
                      Icon(iconName,size: 70.0,color: color,),
                      Text(label,style: const TextStyle(fontSize: 17.0,),),
                    ],
                  ),
                ),
              ),
  );
}